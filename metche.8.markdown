% METCHE(8) metche user manual
% metche and this manual page were written by the boum.org collective, and are now maintained by the metche developers collective <metche@lists.riseup.net>
% June 5, 2011

NAME
====

metche - reducing root bus factor

SYNOPSIS
========

	metche [-h VSERVER] report (stable|testing|unstable)-YYYYMMDDHHMM
	metche [-h VSERVER] list
	metche [-h VSERVER] stabilize testing-YYYYMMDDHHMM

DESCRIPTION
===========

metche is a tool meant to ease collective system administration by
monitoring changes in the system configuration.

metche basic usage is to monitor changes in a directory, usually
`/etc`; optionally, metche can also monitor:

 * one or more user maintained changelog files,
 * the state of Debian packages and versions.

metche should be installed with a cronjob that regularly runs to
automatically save the system state as needed. These states are saved
in a way similar to the Debian development model:

 * _unstable_ states are saved as soon as a change is detected. They
   are kept until a new _testing_ state appears.
 * _testing_ states is created from the last _unstable_ state that has
   not been changed after a short amount of time (by default, one
   hour). Old _unstable_ states are deleted afterwards.
 * _stable_ states are created from the last _testing_ state, either
   manually, or after a long amount of time (by default, 3 days). Old
   _testing_ states are deleted afterwards.

When a new _testing_ state is saved, an email is sent to a
configurable address, giving an overwiew of the differences with the
previous _testing_. A notification is also sent when a new _stable_
state is saved.

metche's configuration is read from `/etc/metche.conf`. Various
settings like changelog monitoring or time between system state
switches are described there.

OPTIONS
=======

If `-h VSERVER` is specified, the VServer _VSERVER_ is operated on
instead of the host system. This, along with the `VSNAMES` option,
allows one to monitor several VServers running on the system.

One of the following commands must be specified on the command line:

report
:   When run with the _report_ command, metche displays a report
    against the specified saved state, or if unspecified, against the
    latest testing state. This is useful when you have broken your
    system and want to know which changes have been made since a
    given, known working, system state.

list
:   When run with the _list_ command, metche displays a list of all
    the saved states.

stabilize
:   When run with the _stabilize_ command, metche turns a "testing
    state" into a "stable state". By default, it will use the last
    "testing state", but this can be overridden by giving a specific
    state as argument.

cron
:   This command should not be called manually, but used from a
    cronjob. When called, it can perform various operations like:
    saving "unstable", "testing" or "stable" states as needed and
    sending reports and notification if configured to do so. This
    command does not support the `-h` option.

FILES
=====

`/etc/metche.conf`  contains metche configuration.

When configured to monitor one changelog, `CHANGELOG_FILE` (default:
`/root/Changelog`).

When configured to monitor multiple changelogs,
`CHANGELOG_DIR/*/Changelog` (default: `/root/changelogs`).

System states are saved in `BACKUP_DIR` (default: `/var/lib/metche`).

SECURITY
========

metche is able to use GnuPG to encrypt the email it sends, but does
not by default; just enable the `ENCRYPT_EMAIL` configuration option,
and make sure `EMAIL_ADDRESS`' public key is in root's keyring,
trusted enough to be used blindly by metche. If `EMAIL_ADDRESS` is an
email alias or mailing-list's address, you probably want to use the
`group` option in `/root/.gnupg/gpg.conf` so that metche reports are
encrypted for every person subscribed to this alias or mailing-list;
for more information, see `gpg(1)`.

When `DO_DETAILS` is enabled and `ENCRYPT_EMAIL` is disabled, metche
sends in _clear text email_ the changes made to the watched
directory... either make sure that the `EXCLUDES` configuration
variable prevents it to send sensitive information, or triple check
that secure connections will be used end-to-end on the email path. If
unsure, set `EMAIL_ADDRESS` configuration variable to a local mailbox.
Please note that `EMAIL_ADDRESS` is not used for VServers:  a
VServer's report messages are sent to its root email address.

metche stores, in `BACKUP_DIR` (default: `/var/lib/metche`), various
backups of `WATCHED_DIR`. Make sure that this backup place is at least
as secured as the source.

BUGS
====

See [metche's ticket system] for known bugs, missing features, and the
development road-map.

[metche's ticket system]: https://0xacab.org/metche/metche/issues
